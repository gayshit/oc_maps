"resources"
{
"maps/oc_atfor_trainyard_v5.bsp" "file"
"maps/oc_atfor_trainyard_v5.res" "file"

"scripts/customweapons/custom_mac10_pistol.txt" "file"
"scripts/customweapons/custom_m249_rifle.txt" "file"
"scripts/customweapons/custom_neagle.txt" "file"

"maps/cfg/oc_atfor_trainyard_v5_briefing.txt" "file"
"maps/cfg/oc_atfor_trainyard_v5_cfg.txt" "file"
"maps/cfg/oc_atfor_trainyard_v5_modify.txt" "file"

"models/weapons/w_mach_m-60para.dx80.vtx" "file"
"models/weapons/w_mach_m-60para.dx90.vtx" "file"
"models/weapons/w_mach_m-60para.mdl" "file"
"models/weapons/w_mach_m-60para.phy" "file"
"models/weapons/w_mach_m-60para.sw.vtx" "file"
"models/weapons/w_mach_m-60para.vvd" "file"

"materials/models/weapons/w_models/Kimono.Twinke.M60/ammobox.vmt" "file"
"materials/models/weapons/w_models/Kimono.Twinke.M60/body2.vmt" "file"
"materials/models/weapons/w_models/Kimono.Twinke.M60/body.vmt" "file"
"materials/models/weapons/w_models/Kimono.Twinke.M60/fore.vmt" 

"materials/models/weapons/v_models/Kimono.Twinke.M60/ammobox.vtf" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/body2.vtf" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/body.vtf" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/fore.vtf"
"materials/models/weapons/v_models/Kimono.Twinke.M60/ammobox_norm.vtf" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/body2_norm.vtf" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/body_norm.vtf" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/fore_norm.vtf"
"materials/models/weapons/v_models/Kimono.Twinke.M60/ammobox.vmt" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/body2.vmt" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/body.vmt" "file"
"materials/models/weapons/v_models/Kimono.Twinke.M60/fore.vmt"
}
